#!/usr/bin/env python3

# 
# This installs the module Kelm2022a and its dependencies.
# The exact revisions used are listed in the table below.
# However, note that this script may also apply further patches.
# If so, all patches are required to be the current folder, or,
# in the one that you specified as argument to this script.
# 
# 
# |      module name      |      branch name      |                 commit sha                 |         commit date         |
# |-----------------------|-----------------------|--------------------------------------------|-----------------------------|
# |  dune-localfunctions  |  origin/releases/2.7  |  68f1bcf32d9068c258707d241624a08b771b6fde  |  2020-11-26 23:45:36 +0000  |
# |      dune-common      |  origin/releases/2.7  |  aa689abba532f40db8f5663fa379ea77211c1953  |  2020-11-10 13:36:21 +0000  |
# |      dune-uggrid      |  origin/releases/2.7  |  1fe87a92c3f7024dd8fb4f3e7764f7a74b794633  |  2021-06-01 20:53:44 +0000  |
# |       dune-grid       |  origin/releases/2.7  |  e8b460122a411cdd92f180dcca81ad8b2ac8d5ee  |  2021-08-31 13:04:20 +0000  |
# |       dune-istl       |  origin/releases/2.7  |  761b28aa1deaa786ec55584ace99667545f1b493  |  2020-11-26 23:29:21 +0000  |
# |     dune-geometry     |  origin/releases/2.7  |  9d56be3e286bc761dd5d453332a8d793eff00cbe  |  2020-11-26 23:26:48 +0000  |

import os
import sys
import subprocess

top = "."
os.makedirs(top, exist_ok=True)


def runFromSubFolder(cmd, subFolder):
    folder = os.path.join(top, subFolder)
    try:
        subprocess.run(cmd, cwd=folder, check=True)
    except Exception as e:
        cmdString = ' '.join(cmd)
        sys.exit(
            "Error when calling:\n{}\n-> folder: {}\n-> error: {}"
            .format(cmdString, folder, str(e))
        )


def installModule(subFolder, url, branch, revision):
    targetFolder = url.rstrip(".git").split("/")[-1]
    if not os.path.exists(targetFolder):
        runFromSubFolder(['git', 'clone', url, targetFolder], '.')
        runFromSubFolder(['git', 'checkout', branch], subFolder)
        runFromSubFolder(['git', 'reset', '--hard', revision], subFolder)
    else:
        print(
            f"Skip cloning {url} since target '{targetFolder}' already exists."
        )


def applyPatch(subFolder, patch):
    sfPath = os.path.join(top, subFolder)
    patchPath = os.path.join(sfPath, 'tmp.patch')
    with open(patchPath, 'w') as patchFile:
        patchFile.write(patch)
    runFromSubFolder(['git', 'apply', 'tmp.patch'], subFolder)
    os.remove(patchPath)

print("Installing dune-localfunctions")
installModule("dune-localfunctions", "https://gitlab.dune-project.org/core/dune-localfunctions.git", "origin/releases/2.7", "68f1bcf32d9068c258707d241624a08b771b6fde", )

print("Installing dune-common")
installModule("dune-common", "https://gitlab.dune-project.org/core/dune-common.git", "origin/releases/2.7", "aa689abba532f40db8f5663fa379ea77211c1953", )

print("Installing dune-uggrid")
installModule("dune-uggrid", "https://gitlab.dune-project.org/staging/dune-uggrid.git", "origin/releases/2.7", "1fe87a92c3f7024dd8fb4f3e7764f7a74b794633", )

print("Installing dune-grid")
installModule("dune-grid", "https://gitlab.dune-project.org/core/dune-grid.git", "origin/releases/2.7", "e8b460122a411cdd92f180dcca81ad8b2ac8d5ee", )

print("Installing dune-istl")
installModule("dune-istl", "https://gitlab.dune-project.org/core/dune-istl.git", "origin/releases/2.7", "761b28aa1deaa786ec55584ace99667545f1b493", )

print("Installing dune-geometry")
installModule("dune-geometry", "https://gitlab.dune-project.org/core/dune-geometry.git", "origin/releases/2.7", "9d56be3e286bc761dd5d453332a8d793eff00cbe", )

print("Configuring project")
runFromSubFolder(
    ['./dune-common/bin/dunecontrol', '--opts=Kelm2022a/cmake.opts', 'all'],
    '.'
)
