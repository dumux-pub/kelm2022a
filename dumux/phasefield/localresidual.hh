// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_PHASEFIELD_LOCAL_RESIDUAL_HH
#define DUMUX_PHASEFIELD_LOCAL_RESIDUAL_HH

#include <dumux/common/properties.hh>
#include <dumux/assembly/cclocalresidual.hh>
#include <dumux/discretization/cellcentered/tpfa/computetransmissibility.hh>
#include "indices.hh"

namespace Dumux {

template<class TypeTag>
class PhasefieldLocalResidual : public CCLocalResidual<TypeTag>
{
    using ParentType = CCLocalResidual<TypeTag>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using Problem = GetPropType<TypeTag, Properties::Problem>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using ElementFluxVariablesCache = typename GetPropType<TypeTag, Properties::GridFluxVariablesCache>::LocalView;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Element = typename GridView::template Codim<0>::Entity;

    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;
    static constexpr int numComponents = ModelTraits::numComponents();

public:
    using ParentType::ParentType;

    /*!
     * \brief Evaluate the rate of change of all conservation
     *        quantites (e.g. phase mass) within a sub-control volume.
     *
     * \param problem The problem
     * \param scv The sub control volume
     * \param volVars The current or previous volVars
     */
    NumEqVector computeStorage(const Problem& problem,
                               const SubControlVolume& scv,
                               const VolumeVariables& volVars) const
    {
        const static Scalar xi = getParam<Scalar>("Problem.xi");
        const static Scalar alpha = problem.getAlpha();
#if !ODE
        const static Scalar delta = getParam<Scalar>("Problem.delta");
        const static Scalar rhoD = getParam<Scalar>("Problem.rhoD");
        const static Scalar rhoP = getParam<Scalar>("Problem.rhoP");
        const static std::array<Scalar, 3> b_D = { -1.0, 1.0, 0.0 };
        const static std::array<Scalar, 3> b_P = { 0.0, 1.0, 1.0 };
#endif
        NumEqVector storage{};
        // Phasefields
        storage[Indices::p1Idx]  = xi*xi*alpha * volVars.priVar(Indices::p1Idx);
        storage[Indices::p2Idx]  = xi*xi*alpha * volVars.priVar(Indices::p2Idx);
        storage[Indices::p3Idx]  = xi*xi*alpha * volVars.priVar(Indices::p3Idx);
#if !ODE
        storage[Indices::uAIdx] = (volVars.priVar(Indices::p1Idx) + delta) *
            volVars.priVar(Indices::uAIdx)
                + rhoP * b_P[0] * volVars.priVars()[Indices::p3Idx]
                + rhoD * b_D[0] * volVars.priVars()[Indices::p2Idx]
                ;
        storage[Indices::uBIdx] = (volVars.priVar(Indices::p1Idx) + delta) *
            volVars.priVar(Indices::uBIdx)
                + rhoP * b_P[1] * volVars.priVars()[Indices::p3Idx]
                + rhoD * b_D[1] * volVars.priVars()[Indices::p2Idx]
                ;
        storage[Indices::uCIdx] = (volVars.priVar(Indices::p1Idx) + delta) *
            volVars.priVar(Indices::uCIdx)
                + rhoP * b_P[2] * volVars.priVars()[Indices::p3Idx]
                + rhoD * b_D[2] * volVars.priVars()[Indices::p2Idx]
                ;
#endif
        return storage;
    }


    /*!
     * \brief Evaluate the flux over a face of a sub control volume.
     *
     * \param problem The problem
     * \param element The element
     * \param fvGeometry The finite volume geometry context
     * \param elemVolVars The volume variables for all flux stencil elements
     * \param scvf The sub control volume face to compute the flux on
     * \param elemFluxVarsCache The cache related to flux computation
     */
    NumEqVector computeFlux(const Problem& problem,
                            const Element& element,
                            const FVElementGeometry& fvGeometry,
                            const ElementVolumeVariables& elemVolVars,
                            const SubControlVolumeFace& scvf,
                            const ElementFluxVariablesCache& elemFluxVarsCache) const
    {
        NumEqVector flux;

        const auto& insideScv = fvGeometry.scv(scvf.insideScvIdx());
        const auto& insideVolVars = elemVolVars[insideScv];
        const auto& outsideVolVars = elemVolVars[scvf.outsideScvIdx()];

        const static Scalar omega = problem.getOmega();
        const static Scalar xi = getParam<Scalar>("Problem.xi");
#if !ODE
        const static Scalar delta = getParam<Scalar>("Problem.delta");
        const static Scalar D_u = getParam<Scalar>("Problem.DiffCoeff");
#endif
        const static std::array<Scalar, numComponents> diffCoeff = {
            xi*xi*omega, xi*xi*omega, xi*xi*omega,// p1 - p3
#if !ODE
            (insideVolVars.priVar(Indices::p1Idx)+delta) * D_u,//u_A
            (insideVolVars.priVar(Indices::p1Idx)+delta) * D_u,
            (insideVolVars.priVar(Indices::p1Idx)+delta) * D_u
#endif
        };

        for (int k = 0; k < numComponents; ++k)
        {
            const auto valInside = insideVolVars.priVar(k);
            const auto valOutside = outsideVolVars.priVar(k);

            Scalar tij;

            const Scalar ti = computeTpfaTransmissibility(scvf, insideScv, diffCoeff[k],
                                                          insideVolVars.extrusionFactor());

            if (scvf.boundary())
                tij = scvf.area()*ti;
            else
            {
                const auto outsideScvIdx = scvf.outsideScvIdx();
                const auto& outsideScv = fvGeometry.scv(outsideScvIdx);
                const Scalar tj = -computeTpfaTransmissibility(scvf, outsideScv, diffCoeff[k],
                                                               outsideVolVars.extrusionFactor());

                if (ti*tj <= 0.0)
                    tij = 0.0;
                else
                    tij = scvf.area()*(ti * tj)/(ti + tj);
            }

            flux[k] = tij*(valInside - valOutside);
        }
        // Implementation for curvature cancellation, activate loop for desired phasefield indices
        const static Scalar numPhasefields = 0;
        const auto insideScvIdx = scvf.insideScvIdx();
        const auto outsideScvIdx = scvf.outsideScvIdx();
        static auto normalGradient = scvf.unitOuterNormal();
        for (int k = 0; k < numPhasefields; ++k)
        {
            normalGradient = 0.0;
            auto elementGrad = 0.0 * scvf.unitOuterNormal();
            for (auto& face : scvfs(fvGeometry))
            {
                Scalar tij;
                Scalar valInside, valOutside;
                const auto finIdx  = face.insideScvIdx();
                const auto& finScv  = fvGeometry.scv(finIdx);
                const auto& finVolVars  = elemVolVars[finScv];
                valInside  = finVolVars.priVar(k);
                const Scalar ti = computeTpfaTransmissibility(face, finScv, 1.0,
                                                              finVolVars.extrusionFactor());
                if (face.boundary())
                {
                    tij = ti;
                }
                else
                {
                    const auto foutIdx = face.outsideScvIdx();
                    const auto& foutScv = fvGeometry.scv(foutIdx);
                    const auto& foutVolVars = elemVolVars[foutScv];
                    valOutside = foutVolVars.priVar(k);
                    const Scalar tj = -computeTpfaTransmissibility(face, foutScv, 1.0,
                                                                   foutVolVars.extrusionFactor());
                    // only gradient desired, no actual flux. Diffusion tensor = 1, area = 1
                    if (ti*tj <= 0.0)
                        tij = 0.0;
                    else
                        tij = (ti * tj)/(ti + tj);
                    // Select original face. One index is always the same but we don't know which
                    if (finIdx + foutIdx ==  insideScvIdx + outsideScvIdx)
                    {
                        normalGradient = tij * (valInside - valOutside) * face.unitOuterNormal();
                    }
                    elementGrad += tij * (valInside - valOutside) * face.unitOuterNormal()/2.0;
                }
            }
            {
            // This should actually be calculated for a face and with face.unitOuterNormal
            // but the only difference might be a sign.
            Scalar localNorm = (normalGradient + elementGrad
                    - (scvf.unitOuterNormal()*elementGrad) * scvf.unitOuterNormal()
                                ).two_norm();
            if (localNorm > 1e-10)
                flux[k] *= (1.0 - elementGrad.two_norm()/localNorm);
            }
        }
        return flux;
    }
};

} // end namespace Dumux

#endif
