// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
#ifndef DUMUX_PHASEFIELD_IO_FIELDS_HH
#define DUMUX_PHASEFIELD_IO_FIELDS_HH

#include "indices.hh"

namespace Dumux {

class PhasefieldIOFields
{
public:
    template <class OutputModule>
    static void initOutputModule(OutputModule& out)
    {
        using Indices = PhasefieldIndices<>;
        out.addVolumeVariable([](const auto& volVars){ return volVars.priVar(Indices::p1Idx); }, "phi1");
        out.addVolumeVariable([](const auto& volVars){ return volVars.priVar(Indices::p2Idx); }, "phi2");
        out.addVolumeVariable([](const auto& volVars){ return volVars.priVar(Indices::p3Idx); }, "phi3");
#if !ODE
        out.addVolumeVariable([](const auto& volVars){ return volVars.priVar(Indices::uAIdx); }, "u_A");
        out.addVolumeVariable([](const auto& volVars){ return volVars.priVar(Indices::uBIdx); }, "u_B");
        out.addVolumeVariable([](const auto& volVars){ return volVars.priVar(Indices::uCIdx); }, "u_C");
#endif
        out.addVolumeVariable([](const auto& volVars){ return volVars.priVar(Indices::p1Idx) + volVars.priVar(Indices::p2Idx) + volVars.priVar(Indices::p3Idx) - 1; }, "sum - 1");
#if !ODE
        out.addVolumeVariable([](const auto& volVars){ return volVars.priVar(Indices::p1Idx) *
                volVars.priVar(Indices::uAIdx); }, "p1 * u_A");
        out.addVolumeVariable([](const auto& volVars){ return volVars.priVar(Indices::p1Idx) *
                volVars.priVar(Indices::uBIdx); }, "p1 * u_B");
        out.addVolumeVariable([](const auto& volVars){ return volVars.priVar(Indices::p1Idx) *
                volVars.priVar(Indices::uCIdx); }, "p1 * u_C");
#endif
    }

    template <class ModelTraits = void, class FluidSystem = void, class SolidSystem = void>
    static std::string primaryVariableName(int pvIdx = 0, int state = 0)
    {
        using Indices = PhasefieldIndices<>;
        switch(pvIdx)
        {
            case Indices::p1Idx: return "phi1";
            case Indices::p2Idx: return "phi2";
            case Indices::p3Idx: return "phi3";
#if !ODE
            case Indices::uAIdx: return "u_A";
            case Indices::uBIdx: return "u_B";
            case Indices::uCIdx: return "u_C";
#endif
        }
    }
};

} // end namespace Dumux

#endif
