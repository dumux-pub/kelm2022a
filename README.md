## Content

This module contains the necessary code to reproduce the base simulation results for the phasefield
implementation as presented in

M. Kelm, S. Gärttner, C. Bringedal, B. Flemisch, P. Knabner, N. Ray:
Comparison Study of phase-field and level-set method for three-phase
systems including two minerals. Comput Geosci 26, 545-570 (2022),
[https://doi.org/10.1007/s10596-022-10142-w](https://doi.org/10.1007/s10596-022-10142-w).

You can configure the module just like any other DUNE
module by using `dunecontrol`. For building and running the executables,
please go to the build folders corresponding to the included tests.


## Version Information

|      module name      |      branch name      |                 commit sha                 |         commit date         |
|-----------------------|-----------------------|--------------------------------------------|-----------------------------|
|  dune-localfunctions  |  origin/releases/2.7  |  68f1bcf32d9068c258707d241624a08b771b6fde  |  2020-11-26 23:45:36 +0000  |
|      dune-common      |  origin/releases/2.7  |  aa689abba532f40db8f5663fa379ea77211c1953  |  2020-11-10 13:36:21 +0000  |
|       dune-grid       |  origin/releases/2.7  |  e8b460122a411cdd92f180dcca81ad8b2ac8d5ee  |  2021-08-31 13:04:20 +0000  |
|       dune-istl       |  origin/releases/2.7  |  761b28aa1deaa786ec55584ace99667545f1b493  |  2020-11-26 23:29:21 +0000  |
|     dune-geometry     |  origin/releases/2.7  |  9d56be3e286bc761dd5d453332a8d793eff00cbe  |  2020-11-26 23:26:48 +0000  |

## Installation

The installation procedure is done as follows:
Create a root folder, e.g. `DUMUX`, enter the previously created folder,
clone this repository and use the install script `install_Kelm2022a.py`
provided in this repository to install all dependent modules.


```sh
mkdir DUMUX
cd DUMUX
git clone https://git.iws.uni-stuttgart.de/dumux-pub/kelm2022a
./Kelm2022a/install_Kelm2022a.py
```

This will clone all modules into the directory `DUMUX`,
configure your module with `dunecontrol` and build tests.

## Building executables and running simulations

All executables will be built and executed from within the module's build directory, in
`Kelm2022a/build-cmake/test/`.
To build the executables for the testcases with or without flow navigate to the subfolders
`freeflow/navierstokes/channel/2d` and `phasefield/allencahn` respectively. To build the executables
run `make test_phasefield_flow`, `make test_phasefield_ode` or `make test_phasefield_pde`.

The simulations are run by executing the resulting binaries, producing .vtu and .pvd output as well
as text files for additional scalar measures. Further postprocessed results such as the diffusivity
and permeability computed through cell problems is not included in this module.
A customized copy of the input parameter file can be passed as an optional argument, e.g.
```sh
./test_phasefield_flow custom_params.input
```

## Authors

In addition to the authors listed in [AUTHORS.md](AUTHORS.md) who are responsible for aspects
related to phase-field models and the paper mentioned above, this module contains code extracted
from [DuMux](https://dumux.org), written by its authors.

## License

This project is licensed under the terms and conditions of the GNU General Public License (GPL)
version 3 or - at your option - any later version. The GPL can be found in the
[gpl-3.0-or-later.txt](LICENSES/gpl-3.0-or-later.txt) file provided in the LICENSES directory.

